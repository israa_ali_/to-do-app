import 'package:flutter/material.dart';
import 'package:todo_app/ui/MainScreenWidgets/mainscreen.dart';
import 'package:todo_app/ui/screens/startScreen.dart';
//import 'package:todo_app/ui/screens/splashScreen.dart';
main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
    //home:StartScreen(),
home: MainScreen(),

    ) ;
  }
}

import 'package:flutter/material.dart';
import 'package:todo_app/ui/MainScreenWidgets/listbutton.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class CostumDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        // width: MediaQuery.of(context).size.width*0.5,
          child: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  DrawerHeader(
                    padding: EdgeInsets.zero,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                  
                    ),
                    
                    child: Logo(),
                  ),
                  SizedBox(height: 1,),
                  RaisedButton(
                   padding:EdgeInsets.all(15),
                   child: Text('Student', style: TextStyle(fontSize: 20, color: Colors.white),),
                   color: const Color.fromARGB(255, 124, 187, 201),
                  // borderRadius: BorderRadius.circular(25.0),
                    onPressed: () {},
                  ),
                  SizedBox(height: 50),
                  ListButton('Feedback'),
                  ListButton('Contact US'),
                  ListButton('Sign Out'),
                ],
              ),
          ),
    );
  }
}
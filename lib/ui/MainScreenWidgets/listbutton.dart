import 'package:flutter/material.dart';

class ListButton extends StatelessWidget {
  var type;
  ListButton(String s){this.type=s;} 
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(title: Text('$type', 
      style: TextStyle(fontSize: 20),), 
      onTap: (){},
      ),
      color: Colors.grey.shade200,
      margin: EdgeInsets.symmetric(horizontal: 40,vertical: 10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
     );
  }
}
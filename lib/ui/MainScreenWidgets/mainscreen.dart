import 'package:flutter/material.dart';
import 'package:todo_app/ui/MainScreenWidgets/costumdrawer.dart';
import 'package:todo_app/ui/MainScreenWidgets/todolist.dart';
import 'package:todo_app/ui/widgets/addNewTask.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To Do'),
        backgroundColor:  const Color.fromARGB(255, 124, 187, 201),
       
      ),
      drawer: CostumDrawer(),
       body: TodoList(),
       floatingActionButton: FloatingButton(),
      
    );
  }
}
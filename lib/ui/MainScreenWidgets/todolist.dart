import 'package:flutter/material.dart';

enum Priority{
  urgent,
  important,
  medium,
  useless,
}

class DummyTask{
  final String text;
  bool isChecked;
  final Priority priority;
 
  DummyTask({
    this.text,
    this.isChecked = false,
    this.priority = Priority.useless
  });
  
  //  Color getColor()=>{
  //    priority.urgent : Colors.red,
  //    priority.meduim : Colors.blue,
  //    priority.important : Colors.red.shade300,
  //    priority.useless : Colors.yellow
  //  }[this.priority];
}

class TodoList extends StatefulWidget {
  @override
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  
  final tasks = [
    DummyTask(text: "jsdhd" , isChecked: true),
    DummyTask(text: "jsdhd"),
    DummyTask(text: "jsdhd" , isChecked: true, priority: Priority.important),
    DummyTask(text: "jsdhd" , isChecked: true),
  ];

  void _onReorder(int oldIndex, int newIndex){
    setState(() {
     if (newIndex > oldIndex) newIndex -= 1; 
     final DummyTask task = tasks.removeAt(oldIndex);
     tasks.insert(newIndex, task);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ReorderableListView(
         onReorder: (oldIndex, newIndex) {},
         children: <Widget>[
          ...tasks.map(
            (task) => Card(
              key: Key(task.text),
              child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 15),
              title: Text(task.text ,maxLines: 1, overflow: TextOverflow.ellipsis,),
              leading: Checkbox(
                onChanged: (bool value){
                  setState(() {
                   task.isChecked =! task.isChecked; 
                  });
                },
                value: task.isChecked,
              ),
              trailing: Icon(Icons.bookmark ,
              // color: task.getColor(),
               ),
             ),
          ),
          ),
        ],
       );
  }
}
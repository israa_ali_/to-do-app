import 'package:flutter/material.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
       Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Logo(),
              CircularProgressIndicator(),

           
            ],
          ),
        ),
      );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hsvcolor_picker/flutter_hsvcolor_picker.dart';
//import 'package:todo_app/ui/widges/submitButton.dart';
import 'package:todo_app/ui/widgets/submitButton.dart';

class FloatingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        _settingModalBottomSheet(context);
      },
      backgroundColor: const Color.fromARGB(255, 124, 187, 201),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext c) {
        return Scaffold(
          appBar: AppBar(
            title: Text('New Task'),
            backgroundColor: const Color.fromARGB(255, 124, 187, 201),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 28.0,
                ),
                Card(
                  child: TextField(
                      decoration: InputDecoration(
                    labelText: "Task ",
                    border: OutlineInputBorder(),
                  )),
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                ),
                SizedBox(
                  height: 10,
                ),
                FloatingActionButton(
                  child: Icon(Icons.colorize),

                  // textColor: Colors.white  ,
                  onPressed: () {
                    _setColor(context);
                  },
                  backgroundColor: const Color.fromARGB(255, 124, 187, 201),
                ),
                SizedBox(
                  height: 80,
                ),
                DropdownButton(),
                Submit(),
              ],
            ),
          ),
        );
      });
}

void _setColor(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext c) {
        return ColorPicker(
          color: Colors.blue,
          onChanged: (value) {},
        );
      });
}

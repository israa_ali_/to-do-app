
import 'package:flutter/material.dart';

class Button extends StatefulWidget {
    var type ;
  @override
  Button (String s){
    this.type=s;
  }

  @override
  _ButtonState createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  Widget build(BuildContext context) {
    
    return ButtonTheme(
      minWidth: 150.0,
      height: 50.0,
       child :RaisedButton(
            child: Text('${widget.type}' , style: TextStyle(fontSize: 24 , fontFamily: 'Roboto'),
            ),
            textColor: Colors.white,
            color: const Color.fromARGB(255, 124, 187, 201),
            
            

             onPressed: () {},
             shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.circular(25.0),
               ),
             ),
       );

             
            
          
      
   
  }
}